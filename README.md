# Max1284P: Platform files for Arduino to run on ATmega1284P

## What is it?

Everything you need to run Arduino on ATmega1284P.

## Current state

Everything is here and has been initially tested.  It should work fine.  There is more detailed testing which must be completed before calling it completely 'done' though.

The platform now includes optiboot.  This bootloader is better in every way than the previous version, so it is recommended in all cases. 

The Makefile has been configured to create one of 3 bootloaders for the clock speeds 16MHz, 12MHz and 8MHz.
16Mhz boards are able to operate at 5V, 12MHz and 8MHz must run at 3.3V to be in spec.

The baudrate for 8MHz boards has been reduced to make it more reliable.

The LED has been moved to PortC Pin 7 and flashes on code upload.

## Installation

1. Download the [ZIP File]
2. Unzip it a folder called 'hardware' off your sketches directory, e.g. /Users/sketchbook/hardware/mighty-1284p
3. Restart the IDE
4. Select Tools > Board > Mighty 1284p 16MHz using Optiboot
5. To burn the bootloader, follow the Arduino [Bootloader](http://arduino.cc/en/Hacking/Bootloader) instructions.

## Requirements

* Works only on Arduino >= 1.0
* Cannot be burned using [USBtinyISP](http://www.ladyada.net/make/usbtinyisp/).  That programmer cannot flash to chips with >64k flash size.
* ArduinoISP has been used successfully to burn the bootloader, either using the ICSP header or with chip installed on breadboard.

## Thanks to maniacbug for providing original bootloader code

http://maniacbug.wordpress.com/2011/11/27/arduino-on-atmega1284p-4/

## Supported Boards

* Max1284, from Boards menu: 'ATMega1284P 12MHz using Optiboot 3.3V'.


